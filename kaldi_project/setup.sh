#

htk_root=/home/speech/bin/Speech_Tools/htk
openfst_root=/home/speech/bin/kaldi-trunk/tools/openfst
kaldi_root=/home/speech/bin/kaldi-trunk/src
vulcan_root=/home/speech/bin/Speech_Tools/vulcan

PATH=$htk_root/HTKTools:$PATH
PATH=$htk_root/HTKLVRec:$PATH
PATH=$openfst_root/bin:$PATH
PATH=$kaldi_root/bin:$PATH
PATH=$kaldi_root/fstbin/:$PATH
PATH=$kaldi_root/gmmbin/:$PATH
PATH=$kaldi_root/featbin/:$PATH
PATH=$kaldi_root/sgmmbin/:$PATH
PATH=$kaldi_root/sgmm2bin/:$PATH
PATH=$kaldi_root/fgmmbin/:$PATH
PATH=$kaldi_root/latbin/:$PATH
PATH=$kaldi_root/nnetbin/:$PATH
PATH=$vulcan_root/bin/:$PATH
PATH=$vulcan_root/HDecode++/:$PATH
export PATH=$PATH

##
cmvn_dir=exp/cmvn

export dev_feat_setup="feat/dev.39.cmvn.ark"
export test_feat_setup="feat/test.39.cmvn.ark"
export train_feat_setup="feat/train.39.cmvn.ark"
