#include <stdio.h>
#include <stdlib.h>
#include "hmm.h"

#define SEQ_LEN 50
#define NUM_SAMPLE 10000
#define NUM_OBSERV 6

int char_to_int(char c)
{
    return (int)(c - 'A');
}

int main(int argc, char* argv[]) 
{
    HMM hmm;
    int t = 0, i = 0, j = 0, k = 0, iter = 0;
    double sum = 0, tmp_sum = 0;
    FILE *output_mdl, *seq_file;
    double alpha[SEQ_LEN][MAX_STATE], beta[SEQ_LEN][MAX_STATE], gamma[SEQ_LEN][MAX_STATE], epsilon[SEQ_LEN - 1][MAX_STATE][MAX_STATE], gamma_obs_sum[NUM_OBSERV][MAX_STATE];
    char seq[100];
    output_mdl = fopen(argv[4], "w");
    seq_file = fopen(argv[3], "r");
    loadHMM(&hmm, argv[2]);
    iter = atoi(argv[1]);
    //iterative part
    for (; iter > 0; iter--) {
        //init gamma & epsilon
        for (t = 0; t < SEQ_LEN; t++)
            for (i = 0; i < hmm.state_num; i++)
                gamma[t][i] = 0;
        for (k = 0; k < NUM_OBSERV; k++)
            for (i = 0; i < hmm.state_num; i++)
                gamma_obs_sum[k][i] = 0;
        for (t = 0; t < SEQ_LEN - 1; t++)
            for (i = 0; i < hmm.state_num; i++)
                for (j = 0; j < hmm.state_num; j++)
                    epsilon[t][i][j] = 0;
        //seek to start of file
        fseek(seq_file, 0, SEEK_SET);
        //build tables
        while (fscanf(seq_file, "%s", seq) != EOF) {
            //build alpha table
            for (i = 0; i < hmm.state_num; i++) 
                alpha[0][i] = hmm.initial[i] * hmm.observation[char_to_int(seq[0])][i];
            for (t = 0; t < SEQ_LEN - 1; t++) {
                for (j = 0; j < hmm.state_num; j++) {
                    for (sum = 0, i = 0; i < hmm.state_num; i++) 
                        sum += alpha[t][i] * hmm.transition[i][j];
                    alpha[t + 1][j] = sum * hmm.observation[char_to_int(seq[t + 1])][j];
                }
            }
            //build beta table
            for (i = 0; i < hmm.state_num; i++)
                beta[SEQ_LEN - 1][i] = 1;
            for (t = SEQ_LEN - 2; t >= 0; t--) {
                for (i = 0; i < hmm.state_num; i++) {
                    for (beta[t][i] = 0, j = 0; j < hmm.state_num; j++) 
                        beta[t][i] += hmm.transition[i][j] * hmm.observation[char_to_int(seq[t + 1])][j] * beta[t + 1][j];
                }
            }
            //build gamma table
            for (t = 0; t < SEQ_LEN; t++) {
                for (sum = 0, i = 0; i < hmm.state_num; i++) 
                    sum += alpha[t][i] * beta[t][i];
                for (i = 0; i < hmm.state_num; i++) {
                    gamma[t][i] += alpha[t][i] * beta[t][i] / sum;
                    gamma_obs_sum[char_to_int(seq[t])][i] += alpha[t][i] * beta[t][i] / sum;
                }
            }
            //build efthlon table
            for (t = 0; t < SEQ_LEN - 1; t++) {
                for (sum = 0, j = 0; j < hmm.state_num; j++)
                    for (i = 0; i < hmm.state_num; i++) 
                        sum += alpha[t][i] * hmm.transition[i][j] * hmm.observation[char_to_int(seq[t + 1])][j] * beta[t + 1][j];
                for (i = 0; i < hmm.state_num; i++)
                    for (j = 0; j < hmm.state_num; j++)
                        epsilon[t][i][j] += alpha[t][i] * hmm.transition[i][j] * hmm.observation[char_to_int(seq[t + 1])][j] * beta[t + 1][j] / sum;
            }
        }
        //init
        for (sum = 0, i = 0; i < hmm.state_num; i++) 
            hmm.initial[i] = gamma[0][i] / NUM_SAMPLE;
        //transition
        for (i = 0; i < hmm.state_num; i++) {
            for (sum = 0, t = 0; t < SEQ_LEN - 1; t++)
                sum += gamma[t][i];
            for (j = 0; j < hmm.state_num; j++) {
                for (tmp_sum = 0, t = 0; t < SEQ_LEN - 1; t++)
                    tmp_sum += epsilon[t][i][j];
                hmm.transition[i][j] = tmp_sum / sum;
            }
        }
        //observation
        for (k = 0; k < NUM_OBSERV; k++) {
            for (j = 0; j < hmm.state_num; j++) {
                for (sum = 0, tmp_sum = 0, t = 0; t < SEQ_LEN; t++) 
                    sum += gamma[t][j];
                hmm.observation[k][j] = gamma_obs_sum[k][j] / sum;
            }
        }
    }
    dumpHMM(stdout, &hmm);
    dumpHMM(output_mdl, &hmm);
    fclose(output_mdl);
    fclose(seq_file);
    return 0;
}
