#include <stdio.h>
#include "hmm.h"

#define NUM_MDL 5
#define NUM_OBSERV 6
#define SEQ_LEN 50
#define STATE_NUM 6

int char_to_int(char c)
{
    return (int)(c - 'A');
}

int main(int argc, char* argv[])
{
    HMM hmm[5];
    FILE *test_data, *result;
    int t=  0, j = 0, i = 0, max_mdl = 0, mdl_idx = 0;
    double delta[SEQ_LEN][STATE_NUM], max_prob = 0, prob = 0, max = 0;
    char seq[100];
    test_data = fopen(argv[2], "r");
    result = fopen(argv[3], "w");
    load_models(argv[1], hmm, NUM_MDL);
    while (fscanf(test_data, "%s", seq) != EOF) {
        for (max_mdl = 0, max_prob = 0, mdl_idx = 0; mdl_idx < NUM_MDL; mdl_idx++) {
            //build delta table
            for (i = 0; i < STATE_NUM; i++)
                delta[0][i] = hmm[mdl_idx].initial[i] * hmm[mdl_idx].observation[char_to_int(seq[0])][i];
            for (t = 1; t < SEQ_LEN; t++)
                for (j = 0; j < STATE_NUM; j++) {
                    for (max = 0, i = 0; i < STATE_NUM; i++) 
                        if ((prob = delta[t - 1][i] * hmm[mdl_idx].transition[i][j] * hmm[mdl_idx].observation[char_to_int(seq[t])][j]) > max) 
                            max = prob;
                    delta[t][j] = max;
                }
            //find max prob
            for (max = 0, i = 0; i < STATE_NUM; i++) {
                if (delta[SEQ_LEN - 1][i] > max)
                    max = delta[SEQ_LEN - 1][i];
            }
            if (max > max_prob) {
                max_prob = max;
                max_mdl = mdl_idx;
            }                
        }
        fprintf(result, "model_0%d.txt %e\n", max_mdl + 1, max_prob);
    }
    return 0;
}
